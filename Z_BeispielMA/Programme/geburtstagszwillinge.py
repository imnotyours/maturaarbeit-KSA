import random # Zufallgenerator importieren

random.seed() # Generator initialisieren

personen = [] # Personen und Summe initialisieren
summe = 0

for k in range(1,1001): # Simulation 1000 Mal laufen lassen
    zwei = 0 # noch keine Geburtstagszwillinge
    personen = []
    while zwei == 0: 
        # Eine Zahl zwischen 1 und 365 anstelle Geburtsdatum
        # an Liste anhängen:
        personen.append(random.randint(1,365))
        
        for i in range(1,366): # Auf selbe Zahl überprüfen
            anzahl = personen.count(i) 
            
            if anzahl >= 2: # Geburtstagszwillinge gefunden?
                zwei = 1  # Ende der Schleife
    leute = len(personen) # Anzahl Leute zählen
    summe = summe + leute # Zu Gesamtsumme hinzufügen

print("Mittelwert:", summe/1000) # Durchschnitt berechnen.

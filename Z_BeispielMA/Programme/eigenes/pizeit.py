import time, mymodul
liste= [] # intialisieren

def messung(): # ab aus 10.12345ab321 s bestimmen
    try:
        return int((str(time.time()).partition(".")[-1] + \
                    "000")[5:7])

    except:
        return int((str(time.time()).partition(".")[-1] + \
                    "000")[5:7])
        

def generator():
    # pi auf 100 Stellen als String aufnehmen:
    pi = "1415926535897932384626433832795028841971693993751
           058209749445923078164062862089986280348253421170679" 
    return str(pi[messung()] + pi[messung()] + pi[messung()] \
                  + pi[messung()]) # 4 Ziffern aus Zeit bestimmen  


for i in range(0,20000): # 1. Stichprobe

     liste.append(generator())
# Datei schreiben:
mymodul.csv(liste, "pizahlen-Stichprobe-1.txt")

liste = [] # Liste leeren


for i in range(0,20000): # 2. Stichprobe
    liste.append(generator())
# Datei schreiben:
mymodul.csv(liste, "pizahlen-Stichprobe-2.txt") 

    

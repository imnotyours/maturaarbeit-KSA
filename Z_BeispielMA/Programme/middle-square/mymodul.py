
def csv(liste, name): # Schreibt Liste in csv
    datei = open(name, "w")
    for item in range(len(liste)):
        datei.write(str(item + 1) + "; 0." + str(liste[item]) + "\n")
    datei.close()

def bsr(p,q, anzahl, bliste): # Binary shift register
    for i in range(anzahl):
        b = (bliste[-p] + bliste[p-q])%2
        bliste.append(b)
    return bliste

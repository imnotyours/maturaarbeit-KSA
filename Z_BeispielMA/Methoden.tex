% !TEX root = beispielMA.tex
\section{Methode}
\label{sec:methode-1-seite}

\subsection{Begriff der Zufallszahl}
\label{sec:begr-der-zufallsz}

Um in die Thematik der Zufallszahlen einzutauchen, sind zuerst einige
Definitionen und Begriffe von Nöten.  Als erstes stellt sich die
Frage, welche Bedingungen eine Zufallszahl bzw. eine Folge von Zahlen
erfüllen muss, damit sie als zufällig gilt.

Nach \cite{Hayes2012} gibt es drei Eigenschaften, die eine Zufallszahl
vorweisen muss: 
\begin{description}
\item[Unvorhersehbarkeit] Erstens müssen die erzeugten Zahlen
  unvorhersehbar sein. Es darf also nicht wie bei einer klassischen
  mathematischen Funktion ein Wert aus der Definitionsmenge
  $\mathbb{D}$ \textit{eindeutig} auf einen Wert aus der Wertemenge
  $\mathbb{W}$ abgebildet werden.
  
\item[Unkorreliertheit] Zweitens sollen die Zahlen
  unkorreliert sein. Das heisst, dass die Kenntnis einer Zahl nicht die
  Kenntnis der nächsten ermöglichen darf. Diese Bedingung erfüllt
  beispielsweise eine durch Rekursion erzeugte Zahlenfolge $x_{i+1}=
  x_i\cdot q$ mit $x_1, q \in \mathbb{N}$ nicht.

\item[Ausgewogenheit] Drittens muss die
  Verteilung dieser Zahlen ausgewogen sein: Es sollen also weder
  Häufungen noch Senkungen von bestimmten Zahlen auftreten. Teilt man
  den Bereich der erzeugten Zufallszahlen in gleich grosse Gebiete,
  soll die tatsächliche Häufigkeit in etwa der zu erwartenden
  entsprechen.
\end{description}
Illustriert am Beispiel eines Würfels mit den Augenzahlen von eins bis sechs lassen sich die Bedingungen wie
folgt zusammenfassen: Der Ausgang eines Wurfes ist unklar. Die
Kenntnis eines Wurfes lässt nicht auf das Resultat des nächsten schliessen und jede Zahl erscheint mit einer
Wahrscheinlichkeit von etwa $\nicefrac{1}{6}\approx\SI{16.67}{\percent}$.

Bereits mit diesen drei Eigenschaften zeigt sich das Spannungsfeld
der Zufallszahlen: Wie soll ein Zufallsgenerator programmiert werden,
wenn die damit erzeugten Zahlen nicht deterministisch sein dürfen? Die
Krux, dass ein Programm beim selben Input unterschiedliche Outputs
erzeugen soll, scheint unüberwindbar.

Da nicht jeder Zufallsgenerator alle drei Bedingungen erfüllen kann,
werden Zufallszahlen nach \citep[][S.90 f]{Hayes2012} in drei Kategorien eingeteilt:
\begin{description}
\item[Echte Zufallszahlen] erfüllen alle drei Bedingungen.
\item[Pseudozufallszahlen] erfüllen die Bedingung der
  Unvorhersehbarkeit nicht. \enquote{Sie werden nach klar bestimmten
    arithmetischen Regeln erzeugt; deren Kenntnis erlaubt es, die
    komplette Folge zu reproduzieren. Allerdings sind
    Pseudozufallszahlen immer noch unkorreliert und auch -- zumindest
    annähernd -- ausgewogen verteilt.} \citep[][S. 91]{Hayes2012}
\item[Quasizufallszahlen] erfüllen nur noch die Bedingung der
  gleichmässigen Verteilung. Sie sind vorhersehbar und bilden eine
  Regelmässigkeit.
\end{description}
\subsection{Prüfmethoden}
\label{sec:prufmethoden}

\subsubsection{Mathematische Kriterien}
\label{sec:d-gleichv-folg}

Nach dem die Bedingungen für Zufallszahlen definiert sind, muss deren
Überprüfbarkeit geklärt werden.

Die Kriterien nach Unvorhersehbarkeit und Unkorreliertheit lassen
sich schwierig quantitativ beurteilen. In der vorliegenden Arbeit kann
dies nur durch Diskussion der
Methode des Generators und damit qualitativ geschehen.

Anders verhält es sich mit der Bedingung nach Gleichverteilung von
Zufallszahlen: Das Kriterium kann in ein mathematisches Modell überführt
werden und soll eine Vorhersage zur Verteilung ermöglichen.
Nach \citep[][S. 51]{Kolonko2008} wird das Kriterium der
Gleichverteilung durch folgenden Satz beschrieben:

  Es sei $x_n, \textnormal{ } n\geq 0$ eine Zufallszahl der
  Zahlenfolge $X$ im Bereich $[0,1]$.  $X$ ist gleich verteilt in
  [0,1], falls für 
  alle Dimensionen $d \in \mathbb{N}$ und alle $0\leq a_i \leq b_i
  \leq 1,\quad i =0, \dots , d-1$ gilt


  \begin{equation}
    \label{eq:7}
    P(a_i < x_i \leq b_i,\textnormal{ }i = 0, \dots , d-1) = \prod_{i=0}^{d-1}(b_i-a_i).
  \end{equation}

Die Wahrscheinlichkeit $P$, dass eine Zufallsvariable $x_i$ im
Intervall $]a_i,b_i]$ liegt, lässt sich als Produkt der
Grenzen errechnen. Für das
Kartesische Koordinatensystem mit $d=2$ Ebenen ist die
Wahrscheinlichkeit beispielsweise $(b_0-a_0)(b_1-a_1)$. Nimmt man als
Beispiel das Einheitsrechteck, so gilt $(1-0)(1-0) = 1$. Dies folgt
direkt aus der Definition $x_n \in [0,1]$. Verdeutlicht ist die
Gleichung~(\ref{eq:7}) in Abbildung~\vref{fig:pxgleich}.

 \begin{figure}[h]
   \centering
   \includegraphics[width=0.5\textwidth]{Bilder/wahrscheinlichkeit.png}
   \caption{$P(x)$ nach Gleichung~(\ref{eq:7}), dass sich ein Punkt
     $A(x_0|x_1)$ innerhalb der schraffierten Fläche befindet}
   \label{fig:pxgleich}
 \end{figure}

Mit diesem Modell lässt sich die Verteilung auch auf mehr als
zwei Dimensionen prüfen. Dies ist für viele Simulationen und
Anwendungen unverzichtbar \citep[vgl.][S. 90]{Hayes2012}.


Zur Überprüfung dieses Satzes gibt es unterschiedliche Herangehensweisen:
Die Folge kann zum einen analytisch, z.B. grafisch, untersucht werden.
Bei dieser Herangehensweise können jedoch keine harten Aussagen
getroffen werden. Zum anderen können statistischen
Verfahren und Tests angewendet werden. Beide Methoden sollen in dieser
Arbeit zur Beurteilung der Gleichverteilung von Zufallszahlen verwendet werden.

\subsubsection{Grafische Überprüfung}
\label{sec:graf-uberpr}
Will man Zufallsgeneratoren auf Regelmässigkeit beziehungsweise
Periodizität hin prüfen, eignet sich dazu oftmals eine grafische
Analyse. Die erste Möglichkeit besteht darin, klassifizierte Werte aus einer
Stichprobe nach ihrer Häufigkeit in einem Histogramm darzustellen. Ein
Histogramm sagt jedoch noch nichts über eine Periodizität oder
Regelmässigkeit aus. Der (schlechte) Zufallsgenerator

\begin{equation}
  \label{eq:6}
  a_{n+1}=(a_n+1)\mod 10, \quad a_0 = 1
\end{equation}

liefert ein ausgewogenes, wenn auch verdächtig gleichmässiges
Histogramm. Es ist aber auf Anhieb ersichtlich, dass er eine Periode
$m=1,2,3,\dots , 9$ erzeugt und damit als schlechter Zufallsgenerator
klassifiziert werden kann. Aufgrund dieser Überlegungen wird das
Histogramm nicht weiter verwendet.

\begin{figure}[thb]
  \centering
  \includegraphics[width= 0.6\textwidth]{Bilder/kartDar.pdf}  
  \caption{Beispiel einer kartesischen Darstellung der Zahlenfolge
    nach Gleichung~(\ref{eq:6})}
  \label{fig:kartesischeDarstellung}
\end{figure}

Verdeutlicht werden kann eine solche Regelmässigkeit durch die
Darstellung der Zahlenfolge in einem kartesischen Koordinatensystem. 
Der $x$-Wert entspricht dabei dem Erscheinungswert, also erhält $a_0$ den
Wert $x= 0$, $a_1$ den Wert $x=1$ und so weiter. Dem $y$-Wert wird
ein Zehntel des Wertes der Zahl zugewiesen. Der Generator aus Gleichung~\eqref{eq:6} ist zur Verdeutlichung
in Abbildung~\vref{fig:kartesischeDarstellung} in einem
Stichprobenumfang von 100 Werten dargestellt. Die Regelmässigkeit
ist hier auch grafisch offensichtlich.


Durch eine Skalierung der Abszisse bilden die einzelnen Punkte
Linien. Für einen guten Generator sollten die Koordinaten eines
Punktes unabhängig voneinander sein und dadurch auch keine optisch
erkennbare Regelmässigkeit vorweisen. 


Die Vorteile einer grafischen Überprüfung liegen in ihrer Einfachheit
und der Möglichkeit, sich ohne grosses mathematisches Wissen einen
ersten Überblick zu verschaffen. Ein Nachteil ist jedoch, dass dieser
Test keine mathematische Grundlage hat und dadurch nicht objektiv ist.
Ein weiterer Nachteil liegt in der Beschränkung der Dimensionen: Die
Zahlenfolge kann mit dieser Methode nur auf eine bis drei Dimensionen
abgebildet werden.

Eine ausführliche Beschreibung der grafischen Analyse findet sich in
\citep[][S. 57ff]{Kolonko2008}.



\subsubsection{Statistisches Testverfahren}
\label{sec:allg-verf-zur}
Die Überprüfung einer Zahlenfolge auf Zufälligkeit erfolgt im
Allgemeinen durch verschiedene statistische Tests.

Als erstes wird die sogenannte Nullhypothese $H_0$ aufgestellt. Die
Nullhypothese enthält die Grundannahme, beispielsweise das Vorliegen
einer Normalverteilung. Die Stichprobe $X:= \{x_o, \dots, x_{n-1}\}$
wird durch $n$-maliges Aufrufen des Generators erzeugt und eine
Prüfgrösse $T(X)$ ausgewertet. $T(X)$ gibt den Abstand zwischen
dem stochastischen Modell und der Verteilung der Stichprobe $X$ an.
Die Prüfgrösse $T$ bildet zusammen mit dem kritischen Wert
$c\in\mathbb{R}$ den Test $(T,c)$. Ist der Abstand $T(X)$ grösser als
$c$, so wird die Hypothese $H_o$ abgelehnt. Der grosse Unterschied zu
den eher subjektiven grafischen Analysen besteht in der
Berücksichtigung einer möglichen Fehlentscheidung: Der Test wird zum
Niveau $\alpha \in ]0,1[$ durchgeführt, so dass ein fälschlicher
Ablehnungsentscheid der Nullhypothese nur mit der
Fehlerwahrscheinlichkeit $\alpha$ getroffen wird. Die
Wahrscheinlichkeit für eine fälschliche Annahme der Hypothese ist
jedoch meist unbegrenzt.

Typische Werte von $\alpha$ sind 0.05 (5\%-Niveau) und 0.01
(1\%-Niveau). Im weiteren  Verlauf dieser Arbeit werden die Tests auf
dem Niveau $\alpha = 0.05$ durchgeführt.

\textit{Wichtig:} Eine Annahme der Hypothese $H_0$ stellt niemals eine
Bestätigung der Nullhypothese dar, da sie nicht statistisch gesichert
ist. Es kann nur aufgezeigt werden, dass die Stichprobe $X$ nicht im
Widerspruch zu $H_0$ steht \citep[vgl.][S.
67f]{Kolonko2008}.

Nach \citep[][S. 673f]{Gellert1974} gilt folgendes 5-Punkte Verfahren zum
Prüfen von Hypothesen. Durch dieses Verfahren kann die Zufälligkeit
eines Generators zwar nicht bewiesen werden, jedoch spricht ein
Bestehen aller Tests eher für einen \enquote{guten} Zufallsgenerator.
Eine kurze Erläuterung der statistischen Begriffe und Kennwerte findet
sich im Anhang~\vref{sec:stat-kennw}.

\begin{enumerate}
\item Der empirische Mittelwert $\overline{x}$ und der theoretische
  Mittelwert $\mu$ einer normalverteilten\footnote{Nach \citep[S.
    129]{fota11} gelten sämtliche Tests auch für nicht normalverteilte
    Grundgesamtheiten, da $\alpha$ approximativ für $n \rightarrow
    \infty$ gleich ist.}
  Stichprobe vom Umfang $n$ und der Varianz $s^2$ werden mit Hilfe der
  $t$- bzw. \textsc{Student}-Verteilung\footnote{William Sealy
    \textsc{Gosset} unter dem Pseudonym \textsc{Student}, 1876--1937} verglichen.
  $H_0$ besagt, dass sich beide nur zufällig unterscheiden. Diese
  Nullhypothese wird mit der Prüfgrösse $t$,

  \begin{equation}
    \label{eq:17}
    t = \frac{|\overline{x}-\mu|}{s}\sqrt{n}
  \end{equation}

  ausgewertet und mit dem Tabellen- bzw. Tafelwert $t_T$ mit $\alpha$ und dem
  Freiheitsgrad $f=n-1$ verglichen. Eine Annahme zum
  Niveau $\alpha$ wird getroffen, wenn der berechnete Wert kleiner als
  der Tafelwert ist.

\item Es werden zwei unabhängige, normalverteilte  Stichproben $X_1$
  und $X_2$ betrachtet. Die Nullhypothese einer zufälligen
  Unterscheidung von $\overline{x_1}$ und $\overline{x_2}$ zur Fehlerwahrscheinlichkeit
  $\alpha$ ist anzunehmen, falls

  \begin{equation}
    \label{eq:8}
    t = \frac{|\overline{x_1}-\overline{x_2}|}{s_d}\sqrt{\frac{n_1
        n_2}{n_1+n_2}} \quad \textnormal{mit } s_d^2 =
    \frac{s_1^2(n_1-1)+ s_2^2(n_2-1)}{n_1+n_2-2}
  \end{equation}

kleiner als der Tafelwert mit $\alpha$ und dem Freiheitsgrad $f = n_1
+ n_2 -2$ ist.


\item Vergleich von Varianzen $s_1^2$ und $s_2^2$: Eine zufällige
  Unterscheidung der Varianzen zweier normalverteilten, unabhängigen
  Stichproben der Grösse $n$ wird mit der Irrtumswahrscheinlichkeit
  $\alpha$ angenommen, wenn der nach der Testgrösse 

  \begin{equation}
    \label{eq:9}
    F = s_1^2:s_2^2, \qquad s_1^2 > s_2^2
  \end{equation}

  berechnete Wert kleiner als der Tafelwert $F_T$ für
  $\alpha$ und die Freiheitsgrade $f_1 = n_1-1$ und $f_2 = n_2 -1$
  ist\footnote{$F$-Verteilung nach Ronald Aylmer \textsc{Fisher},
    1890--1962}.


\item Die Häufigkeiten einzelner Elemente einer Stichprobe mit dem
  Umfang $n$ werden
  mit der erwarteten Häufigkeit verglichen. Tritt ein Ereignis $z$-mal
  ein, in der Grundgesamtheit aber mit der Wahrscheinlichkeit $p$ ein, so
  weicht die relative Häufigkeit $z:n$ nur zufällig mit einer
  Irrtumswahrscheinlichkeit $\alpha$ von $p$ ab, wenn der Wert der
  Testgrösse
  \begin{equation}
    \label{eq:10}
    t = \frac{|z-np|}{\sqrt{np(1-p)}}
  \end{equation}

kleiner ist als der Tafelwert $t_T$ mit $\alpha$ und $f=n-1$ ist.

\item Als letztes wird die Verteilung mit dem $\chi^2$-Test
  (Chi-Quadrat-Test)\footnote{Karl \textsc{Pearson}, 1857--1936} überprüft. Eine empirische
  Verteilung weicht nur zufällig von der theoretischen ab, wenn der
  berechnete Wert 
  \begin{equation}
    \label{eq:11}
    \chi^2= \sum_{i=1}^k \frac{(h_i -k_i)^2}{k_i}
  \end{equation}
kleiner ist als der Tafelwert $\chi^2_T$ mit $\alpha$ und dem
Freiheitsgrad $f = k-m-1$ ist, $m$ stellt die Anzahl der geschätzten
Parameter dar. Weiter
sind die Werte der Stichprobe in $k$ Klassen geteilt und $h_i$ ist die
beobachtete, $k_i$ hingegen die zu erwartende Häufigkeit. Muss
für die theoretische Häufigkeit $k_i$ beispielsweise nur die Varianz
mithilfe der Stichprobe abgeschätzt werden, gilt $m=1$. 
\end{enumerate}

Für die Durchführung dieser Tests bieten sich diverse
Statistikprogramme an. Für diese Maturaarbeit wurde mit
Minitab$^{\textsuperscript{\textregistered}}$\,17 gearbeitet. Eine kostenlose Demoversion kann unter
\url{http://www.minitab.com/de-de/products/minitab/features/} bezogen
werden. Die Protokolle der Tests finden sich im digitalen Anhang \vref{sec:digitaler-anhang}.

\subsection{Umsetzung}
\label{sec:umsetzung}

\subsubsection{Programmierung}
\label{sec:programmierung}

Um Zufallsgeneratoren innerhalb einer Programmierumgebung zu erzeugen,
ist die Wahl einer Programmiersprache erforderlich. Ich habe mich für
Python (Version 3) entschieden. Python ist eine
objektorientierte Sprache und wurde Anfang der 1990er Jahre von Guido
\textsc{van Rossum} entwickelt, nun wird sie quelloffen und gemeinschaftsbasiert
weiterentwickelt.

Die Sprache ist einfach zu lernen, klar strukturiert, gut lesbar und
plattformunabhängig. Python wird in zahlreichen Unternehmen
verwendet, so ist diese Sprache eine der drei offiziellen
Programmiersprachen von Google und genutzte
Programmiersprache in YouTube \citep[vgl.][S. 17f]{Theis2014}.

Für  Mac OS X und Windows ist unter \url{https://www.python.org/downloads/}
die entsprechende Datei herunterzuladen und zu installieren. Das Buch
\textit{Einstieg in Python} von \cite{Theis2014} bietet für Anfänger
einen guten Einstieg. 

Um Zufallsgeneratoren zu programmieren, werden zum einen zwei
bereits bestehende (historische) Algorithmen sowie ein eigener
Algorithmus in dieser Sprache umgesetzt und verfeinert. Die Programme sind in Anhang~\vref{sec:anhang-programme} ausgeführt und sollen mit Hilfe einer
qualitativen als auch statistischen Auswertung analysiert und nach
ihrer Güte beurteilt werden. Um das Verstehen der Programme zu
erleichtern findet sich dort auch eine kleine Notationshilfe.

\subsubsection{Physische Generatoren}
\label{sec:betr-allt-phan}
Um Zufallszahlen physisch zu erzeugen, werden folgende
Generatoren verwendet:
\begin{description}
\item[Lottozahlen:] 
Kugeln mit den Zahlen 1 - 45 werden aus einer Urne automatisiert
gezogen (ohne zurücklegen). Grundsätzlich liegen zwar die gezogenen Zahlen vom 10. Januar 1970 bis
am 27. September 2014 vor
\citep[vgl.][]{SwisslosInterkantonaleLandeslotterie2014}, jedoch wurde
der Ziehungsmodus mehrmals geändert. Um eine möglichst grosse
Stichprobe auswerten zu können, 
werden die Ziehungen vom 4. Januar 1986 bis am 18. April 2009
betrachtet. Der Ziehungsmodus während dieser Periode war
$\begin{pmatrix}45 \\ 6 \end{pmatrix}$
plus eine Bonuszahl, die aus der gleichen Urne gezogen wurde.

Bei der Auswertung der gezogenen Zahlen ergeben sich mehrere
methodische Probleme:
\begin{itemize}
\item Die Reihenfolge der Zahlen innerhalb einer Ziehung ist unklar.
  Eine zufällige Kombination der einzelnen Zahlen zu einer neuen 
  ist daher nicht möglich.
\item Da die Bonuszahl aus der gleichen Urne gezogen wird, kann sie
  als siebte Zahl betrachtet werden. Die Anzahl Möglichkeiten
  berechnet sich daher aus $
  \begin{pmatrix}
  45 \\ 7
\end{pmatrix}
$.
\end{itemize}

Aufgrund dieser Einschränkungen, wird nur die Zufälligkeit der Zahl 25
ausgewertet. Wird diese gezogen, erhält sie den Wert 1, ansonsten den
Wert 0. Die erzeugte Zahlenfolge kann wie jede andere behandelt
werden, der einzige Unterschied besteht im theoretischen Mittelwert:
\begin{equation}
  \label{eq:18}
  \mu = \frac{
  \begin{pmatrix}
    44 \\ 6
  \end{pmatrix}
\cdot
\begin{pmatrix}
  1 \\ 1
\end{pmatrix}
}{
  \begin{pmatrix}
    45 \\ 7
  \end{pmatrix}
} = \frac{7}{45}=0.1\bar 5
\end{equation}


Es werden also alle möglichen Kombinationen ohne die Zahl 25 durch die
gesamte Zahl der Kombinationen dividiert.
\item[Radioaktiver Zerfall:] Die Literatur, bspw. \citep[][S.
  661]{Gellert1974}, beschreibt den radioaktiven Zerfall als zufällig.
Um dies zu überprüfen, werden mit einer Sonde die Anzahl
der radioaktiven Impulse pro hundertstel Sekunde des Isotops $^{241}$Am
gemessen. Das Americiumisotop hat eine Halbwertszeit von 432,2
Jahren und zerfällt durch einen $\alpha$- und $\gamma$-Zerfall
\citep[vgl.][S. 200]{fota11}.

Der durchschnittliche Zerfall pro Sekunde ist abhängig von
verschiedenen Faktoren wie Alter und Reinheit der Probe, Abstand
zwischen Probe und Sonde. Deshalb gibt es keinen Literaturwert für den
theoretischen Mittelwert $\mu$. Um den empirischen Mittelwert
$\overline{x}$ trotzdem mit einem anderen vergleichen zu können, wird
der Mittelwert der ersten 3.3\% mit dem der gesamten Probe verglichen.

\item[Kaugummiverteilung:]
Vor dem Haupteingang der Kanti spucken die viele SchülerInnen die
Kaugummis auf den Boden. Es wird geprüft, ob das daraus entstehende
Muster zufälliger Art ist oder nicht. Dazu wir über eine Fotografie
den auf dem Boden liegenden Kaugummis ein
Koordinatensystem gelegt. Anschliessend werden auf dem Computer
mit Hilfe eines Excel-Makros \citep{cva} die Koordinaten der einzelnen weissen
Punkte bestimmt. Um diese nun in eine
eindimensionale Abfolge zu bringen, werden die $x$-Koordinaten nach
den $y$-Koordinaten sortiert.
\end{description}

Die durch physische Generatoren erzeugten Zahlen werden wie die
Algorithmen den statistischen Tests unterworfen. 



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Bericht"
%%% End: 

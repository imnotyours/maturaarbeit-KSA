[windows]: https://tug.org/texlive/windows.html
[osx]: https://tug.org/mactex/
[texshop]: https://pages.uoregon.edu/koch/texshop/texshop.html
[projekt]: //gitlab.com/c.v.a/maturaarbeit-KSA/-/archive/master/maturaarbeit-KSA-master.zip
[vorlage]: https://gitlab.com/c.v.a/maturaarbeit-KSA/tree/master/MA-Vorlage
[beispiel]: https://gitlab.com/c.v.a/maturaarbeit-KSA/tree/master/BeispielMA
[jabref]: https://www.fosshub.com/JabRef.html
[cva]: mailto:christoph.vonarx@edulu.ch
[wiki]: https://gitlab.com/c.v.a/maturaarbeit-KSA/wikis/home
[tobiaspaul]:https://www.tobiaspaul.net/dokuwiki/doku.php?id=software:latex:tipps_tricks
[bibdesk]:https://bibdesk.sourceforge.io/

## maturaarbeit-KSA

LaTeX Vorlage für die Maturaarbeit an der Kantonsschule Alpenquai Luzern, CH  
2018, Christoph von Arx, [christoph.vonarx@edulu.ch][cva]  

[FAQ (Wiki)][wiki]

#### 1.  Download einer LaTeX Umgebung (TeX Live ist empfehlenswert) und eines Programms für die Quellen (JabRef).
TeX Live:
* [Windows][windows], Editor: TeX Works, schon inbegriffen
* [OS X][osx], Editor: [TeXShop][texshop], muss separat runtergeladen werden
* Linux `sudo apt-get install texlive texlive-lang-german texlive-latex-extra`  
(siehe auch [Tips und Tricks][tobiaspaul])

> Wichtig: der Editor muss auf die Textcodierung UTF-8 eingestellt sein.

Datenbank für das Erfassen der Quellen:
* [JabRef][jabref] (alle Plattformen)  
* [BibDesk][bibdesk] (OS X)  

> Wichtig: auch JabRef muss auf die Textcodierung UTF-8 eingestellt sein.



#### 2.  Download der MA-Arbeit
 * [Ganzes Projekt][projekt] (als zip-Datei)
 
 Einzelne Dateien:
 * [Vorlage][vorlage]
 * [Beispiel Maturaarbeit][beispiel] 

#### 3.  Mit Schreiben beginnen
 * MA.tex mit **XeLaTex** setzen, ausprobieren wie vom PDF zum Quelltext hin und her gewechselt wird
 * MA.tex ausfüllen
 * Die Maturaarbeit via Inhaltsverzeichnis aufbauen:
    * Erste Ebene: Kapiteltitel bearbeiten (`\section{}`)  
    * Zweite Ebene: Unterkapitel einfügen (`\subsection{}`)
    * Dritte Ebene: Unterunterkapitel einfügen (`\subsubsection{}`) und mit Text füllen.
 
 > Tip: Am Anfang häufig setzen. Wenn der Satz nicht durchläuft, versuchen Sie den Fehler zu finden und zu korrigieren.

